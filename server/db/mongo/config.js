export const db = process.env.MONGO_URL || process.env.MONGODB_URI || 'mongodb://localhost/volleto';

export default {
  db
};
