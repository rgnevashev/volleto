/**
 * Schema Definitions
 */
import mongoose from 'mongoose';

const ResultSchema = new mongoose.Schema({
  //id: String,
  originalImageUrl: String, // Secure_url of the uploaded image,
  serialNumberImageUrl: String, // Secure_url of the uploaded image,
  referenceNumberImageUrl: String, // Secure_url of the uploaded image,
  serialNumber: String,
  referenceNumber: String,
  createdAt: { type: Date, default: Date.now }
});

// Compiles the schema into a model, opening (or creating, if
//  nonexistent) the 'Topic' collection in the MongoDB database
export default mongoose.model('results', ResultSchema);

