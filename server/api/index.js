/**
 * Routes for express app
 */
import * as resultsApi from './results'

export default (app) => {
  // Results
  app.get('/results', resultsApi.all)
  app.post('/result/create', resultsApi.save)
  // app.put('/result/:resultId/update', resultsApi.update)
  // app.delete('/result/:resultId', resultsApi.delete)
}
