import { Results } from '../../db/mongo'

export const all = (req, res) => {
  Results.find({}).exec((err, results) => {
    if (err) {
      console.log('Error in first query');
      return res.status(500).send('Something went wrong getting the data');
    }

    return res.json(results);
  });
}

export const save = (req, res) => {
  req.body.id = ''
  Results.create(req.body, (err) => {
    if (err) {
      console.log(err);
      return res.status(400).send(err);
    }

    return res.status(200).send('OK');
  });
}
