import './config'

import app from './express'

import './db'

import routes from './routes'

// Init routes
routes(app)

// Listen app
app.listen(app.get('port'), () => {
  console.log('Example app listening on port 3000!');
});
