import apiInit from '../../api'
import reactRouterMiddleware from './middleware'

export default (app) => {
  /*
   * API REST Server
   */
  apiInit(app)

  /**
  * Another routes here...
  */

  /*
   * This is where the magic happens. We take the locals data we have already
   * fetched and seed our stores with data.
   * reactRouterMiddleware matches the URL with react-router and renders the app into
   * HTML
   */
  app.get('*', reactRouterMiddleware)

  /*
  app.get('/', function (req, res) {
    res.send('Hello World!');
  });*/
}
