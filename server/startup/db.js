import mongoConnect from '../db/mongo/connect';

/*
 * Database-specific setup
 * - connect to MongoDB using mongoose
 * - register mongoose Schema
 */
mongoConnect();
