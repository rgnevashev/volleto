import { HOST, PORT, ENV } from './env'

class App {
  constructor() {
    this.HOST = HOST
    this.PORT = PORT
    this.ENV = ENV
    this.baseURL = `http://${HOST}:${PORT}`
    this.isClient = typeof window !== 'undefined'
  }
}

export default new App()
