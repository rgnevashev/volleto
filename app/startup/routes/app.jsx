import React from 'react';
import { Route, IndexRoute } from 'react-router';

import { MainLayout } from '../../ui/layouts'

// TODO later
// import {  } from '../../ui/containers'

import { Home, ImageAdd, ImageCaptured, ImageCropSerialNum, ImageCropRefNum, ImageResults } from '../../ui/components'

/*
 * @param {Redux Store}
 * We require store as an argument here because we wish to get
 * state from the store after it has been authenticated.
 */
export default (store) => {
  return (
    <Route path="/" component={MainLayout}>
      <IndexRoute component={Home} />
      <Route path="image-add" component={ImageAdd} />
      <Route path="image-captured" component={ImageCaptured} />
      <Route path="image-crop-serial-num" component={ImageCropSerialNum} />
      <Route path="image-crop-ref-num" component={ImageCropRefNum} />
      <Route path="image-results" component={ImageResults} />
      {/*<Route path="login" component={LoginOrRegister} />*/}
    </Route>
  )
}
