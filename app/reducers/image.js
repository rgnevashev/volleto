import { combineReducers } from 'redux'

// { type: 'IMAGE_UPLOAD_FROM_FILE', file: {} }
// { type: 'IMAGE_CROP_SERIAL_NUM', area: {} }
// { type: 'IMAGE_CROP_REFERENCE_NUM', area: {} }
// { type: 'IMAGE_PROCESSING' }
// { type: 'IMAGE_RESULTS',  }

// TODO replace to cloudinary node
const CLOUDINARY_VIEW_URL = 'https://res.cloudinary.com/rgnevashev/image/upload'
const buildUrl = (originalUrl, area) => {
  const params = [
    'c_crop',
    `w_${parseInt(area.width, 10)}`,
    `h_${parseInt(area.height, 10)}`,
    `x_${parseInt(area.x, 10)}`,
    `y_${parseInt(area.y, 10)}`
  ]
  return `${CLOUDINARY_VIEW_URL}/${params.join(',')}/${originalUrl.split('/image/upload/')[1]}`
}

const image = (state = {}, action) => {
  switch (action.type) {
    case 'IMAGE_UPLOAD_FROM_FILE':
      return {
        ...state,
        originalImageUrl: action.file.url
      }
    case 'IMAGE_CROP_SERIAL_NUM':
      const imageCropSerialNumArea = action.area
      const serialNumberImageUrl = buildUrl(state.originalImageUrl, action.area)
      return {
        ...state,
        imageCropSerialNumArea,
        serialNumberImageUrl
      }
    case 'IMAGE_CROP_REFERENCE_NUM':
      const imageCropRefNumArea = action.area
      const referenceNumberImageUrl = buildUrl(state.originalImageUrl, action.area)
      return {
        ...state,
        imageCropRefNumArea,
        referenceNumberImageUrl
      }
    case 'IMAGE_PROCESSING':
      return {
        ...state,
        serialNumber: action.serialNumber,
        referenceNumber: action.referenceNumber
      }
    case 'IMAGE_RESULTS':
      console.log(state, action)
      return state
    default:
      return state
  }
}

const imageReducer = combineReducers({
  image
})

export default imageReducer
