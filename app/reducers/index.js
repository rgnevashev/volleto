import { combineReducers } from 'redux'
import { routerReducer as routing } from 'react-router-redux'

import imageReducer from './image'


const rootReducer = combineReducers({
  imageReducer,
  routing,
})

export default rootReducer
