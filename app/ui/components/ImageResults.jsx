// ImageResults
import React from 'react'
import request from 'superagent'
import { Link } from 'react-router'

class ImageResultsComponent extends React.Component {

  constructor(props, { store }) {
    super(props)

    this.store = store

    this.state = {
      approved: false
    }
  }

  imageApprove() {
    const { imageReducer: { image } } = this.store.getState()
    this.setState({ approved: true })
    request.post('/result/create')
      .send({
        originalImageUrl: image.originalImageUrl,
        serialNumberImageUrl: image.serialNumberImageUrl,
        referenceNumberImageUrl: image.referenceNumberImageUrl,
        serialNumber: image.serialNumber,
        referenceNumber: image.referenceNumber
      })
      .then(result => {
        this.setState({ approved: true })
        this.store.dispatch({
          type: 'IMAGE_RESULTS'
        })
      })
      .catch(err => console.error(err))
  }

  render() {
    const { approved } = this.state
    const { imageReducer: { image } } = this.store.getState()

    return (
      <div>
        <h2>Results</h2>
        <h1>Serial Number: <br />{image.serialNumber}</h1>
        <h1>Reference Number: <br />{image.referenceNumber}</h1>
        {approved ?
          <div>
            <h2 className="text-success">Thank you</h2>
            <Link className="btn btn-primary btn-block" to="/image-add">
              Restart
            </Link>
          </div> :
          <div>
            <div>
              <img src={image.originalImageUrl} />
            </div>
            <br />
            <div className="row">
              <div className="col-sm-6">
                <Link className="btn btn-primary btn-block" to="/image-crop-serial-num">
                  Wrong
                </Link>
              </div>
              <div className="col-sm-6">
                <Link className="btn btn-primary btn-block" onClick={() => this.imageApprove()}>
                  Approve
                </Link>
              </div>
            </div>
          </div>
        }
      </div>
    )
  }
}

ImageResultsComponent.contextTypes = {
  store: React.PropTypes.any
}

export default ImageResultsComponent
