import React from 'react'
import request from 'superagent'
import { Link } from 'react-router'
import Dropzone from 'react-dropzone'

const styles = {
  width: '100%',
  height: 100,
  border: '2px dashed #666',
  borderRadius: '5px'
}

const CLOUDINARY_UPLOAD_PRESET = 'fi6hhtpf';
const CLOUDINARY_UPLOAD_URL = 'https://api.cloudinary.com/v1_1/rgnevashev/upload';

class ImageAddComponent extends React.Component {

  constructor(props, { store }) {
    super(props)

    this.store = store

    this.state = {

    }
  }

  onDrop(files) {
    this.setState({
      uploadedFile: files[0]
    });

    this.handleImageUpload(files[0]);
  }

  handleImageUpload(file) {
    request.post(CLOUDINARY_UPLOAD_URL)
      .attach('file', file)
      .field('upload_preset', CLOUDINARY_UPLOAD_PRESET)
      .then((response) => {
        this.store.dispatch({ type: 'IMAGE_UPLOAD_FROM_FILE', file: response.body })
        this.props.router.push('/image-captured')
      })
      .catch(err => console.error(err))
    /*{"public_id":"tests/1_zhj1fv","version":1487808948,"signature":"ce6b9db746ff7d59186a84f5b6027763fcf4a9d5","width":797,"height":400,"format":"jpg","resource_type":"image","created_at":"2017-02-23T00:15:48Z","tags":[],"bytes":127547,"type":"upload","etag":"0262e6a1f64bbcf58e255a10ad4aad9e","url":"http://res.cloudinary.com/rgnevashev/image/upload/v1487808948/tests/1_zhj1fv.jpg","secure_url":"https://res.cloudinary.com/rgnevashev/image/upload/v1487808948/tests/1_zhj1fv.jpg","access_mode":"public","existing":false,"image_metadata":{"JFIFVersion":"1.01","ResolutionUnit":"None","XResolution":"1","YResolution":"1","Colorspace":"RGB"},"original_filename":"1"}"*/
  }

  render() {
    return (
      <div>
        <div style={{ marginBottom: 20 }}>
          <Dropzone onDrop={files => this.onDrop(files)} multiple={false} accept="image/*" style={styles}>
            <div>Drop an image or click to select a file to upload</div>
          </Dropzone>
        </div>
        <div>
          - Take image
          - Select from Gallery
          - Image URL
          - Select from Google Drive
        </div>
      </div>
    )
  }
}

ImageAddComponent.contextTypes = {
  store: React.PropTypes.any
}

export default ImageAddComponent
