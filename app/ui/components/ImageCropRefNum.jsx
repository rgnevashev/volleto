// ImageCropRefNum
import React from 'react'
import Cropper from 'react-cropper'
import request from 'superagent'
import { Link } from 'react-router'

const cognitiveImage = (url, done) => {
  request.post('https://westus.api.cognitive.microsoft.com/vision/v1.0/ocr?language=unk&detectOrientation=true')
    .set('Ocp-Apim-Subscription-Key', 'fae7a34aef1a4d52900e827dd04eea79')
    .set('Content-Type', 'application/json')
    .send({ url })
    .then((result) => {
      if (done) {
        const text = result.body.regions[0].lines[0].words.map(word => word.text).join(' ')
        done(text)
      }
    })
    .catch(err => console.error(err))
}

class ImageCropRefNumComponent extends React.Component {

  constructor(props, { store }) {
    super(props)

    this.store = store

    this.state = {
      processing: false
    }
  }

  onCrop() {
    this.store.dispatch({
      type: 'IMAGE_CROP_REFERENCE_NUM',
      area: this.refs.cropper.cropper.getData()
    })
  }

  imageProcess() {
    const { imageReducer: { image } } = this.store.getState()
    this.setState({ processing: true })
    console.log(image.serialNumberImageUrl)
    console.log(image.referenceNumberImageUrl)
    cognitiveImage(image.serialNumberImageUrl, (serialNumber) => {
      console.log(serialNumber)
      cognitiveImage(image.referenceNumberImageUrl, (referenceNumber) => {
        console.log(referenceNumber)
        this.store.dispatch({
          type: 'IMAGE_PROCESSING',
          serialNumber,
          referenceNumber
        })
        this.setState({ processing: false })
        this.props.router.push('/image-results')
      })
    })
  }

  render() {
    const { processing } = this.state
    const { imageReducer: { image } } = this.store.getState()

    return (
      <div>
        {processing ?
          <div>
            <h2>Processing...</h2>
          </div> :
          <div>
            <h1>Reference Number<br /><small>Focus on the reference number. Such as XYZ.TDO.BRR</small></h1>
            <div>
              <Cropper
                ref='cropper'
                src={image.originalImageUrl}
                style={{height: 400, width: '100%'}}
                minCropBoxHeight={50}
                minCropBoxWidth={50}
                crop={() => this.onCrop()}
              />
            </div>
            <br />
            <div className="row">
              <div className="col-sm-6">
                <Link className="btn btn-primary btn-block" to="/image-add">
                  Restart
                </Link>
              </div>
              <div className="col-sm-6">
                <Link className="btn btn-primary btn-block" onClick={() => this.imageProcess(image)}>
                  Next
                </Link>
              </div>
            </div>
          </div>
        }
      </div>
    )
  }
}

ImageCropRefNumComponent.contextTypes = {
  store: React.PropTypes.any
}

export default ImageCropRefNumComponent
