import React from 'react'
import { Link } from 'react-router'

class HomeComponent extends React.Component {

  render() {
    return (
      <div>
        <h1>Capture the back of your watch</h1>
        <div>
          <img src="//placehold.it/300x300" />
        </div>
        <br />
        <Link className="btn btn-primary btn-block" to="/image-add">
          Add Image
        </Link>
      </div>
    )
  }
}

export default HomeComponent
