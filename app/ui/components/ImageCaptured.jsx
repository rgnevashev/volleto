import React from 'react'
import { Link } from 'react-router'

class ImageCapturedComponent extends React.Component {

  constructor(props, { store }) {
    super(props)

    this.store = store

    this.state = {

    }
  }

  render() {
    const { imageReducer: { image } } = this.store.getState()

    return (
      <div>
        <h1>Is it good enought?</h1>
        <div>
          <img src={image.originalImageUrl} />
        </div>
        <br />
        <div className="row">
          <div className="col-sm-6">
            <Link className="btn btn-primary btn-block" to="/image-add">
              Retake Image
            </Link>
          </div>
          <div className="col-sm-6">
            <Link className="btn btn-primary btn-block" to="/image-crop-serial-num">
              Next
            </Link>
          </div>
        </div>
      </div>
    )
  }
}

ImageCapturedComponent.contextTypes = {
  store: React.PropTypes.any
}

export default ImageCapturedComponent
