// ImageCropSerialNum
import React from 'react'
import Cropper from 'react-cropper'
import { Link } from 'react-router'

class ImageCropSerialNumComponent extends React.Component {

  constructor(props, { store }) {
    super(props)

    this.store = store

    this.state = {

    }
  }

  onCrop() {
    this.store.dispatch({
      type: 'IMAGE_CROP_SERIAL_NUM',
      area: this.refs.cropper.cropper.getData()
    })
  }

  render() {
    const { imageReducer: { image } } = this.store.getState()

    return (
      <div>
        <h1>Serial Number <br /><small>Crop to leave only the serial number. Such as 123456</small></h1>
        <div>
          <Cropper
            ref='cropper'
            src={image.originalImageUrl}
            style={{height: 400, width: '100%'}}
            minCropBoxHeight={50}
            minCropBoxWidth={50}
            crop={() => this.onCrop()}
          />
        </div>
        <br />
        <div className="row">
          <div className="col-sm-6">
            <Link className="btn btn-primary btn-block" to="/image-add">
              Retake Image
            </Link>
          </div>
          <div className="col-sm-6">
            <Link className="btn btn-primary btn-block" to="/image-crop-ref-num">
              Next
            </Link>
          </div>
        </div>
      </div>
    )
  }
}

ImageCropSerialNumComponent.contextTypes = {
  store: React.PropTypes.any
}

export default ImageCropSerialNumComponent
