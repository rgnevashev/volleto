import React from 'react'

const Header = () => (
  <div className="header">
    <small>watch detector<br />-Alpha-</small>
  </div>
)

const Footer = () => (
  <div className="footer">
    <small>Do not try this at home<br />ver 0.0.1</small>
  </div>
)

class MainLayout extends React.Component {

  render() {
    return (
      <div className="container text-center">
        <Header />
        {this.props.children}
        <Footer />
      </div>
    )
  }
}

export default MainLayout
