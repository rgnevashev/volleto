/* eslint-disable */
const path = require('path');
const CURRENT_WORKING_DIR = process.cwd();

module.exports = require("@terse/webpack").api()
  .sourcemap('source-map')
  .node({
    "__dirname": true,
    "__filename": true
  })
  .loader("babel-loader", [".js",".jsx"], {
    exclude: path.resolve(process.cwd(), 'node_modules'),
    query: {
      cacheDirectory: true,
      compact: true,
      presets: [
        "es2015",
        "react",
        "stage-0"
      ],
      plugins: [
        "transform-react-remove-prop-types",
        "transform-react-constant-elements",
        "transform-react-inline-elements"
      ]
    }
  })

  .loader('url-loader?limit=100000', /\.(png|jpg|jpeg|gif|svg|woff|woff2)$/)
  .loader("json-loader", ".json")
  .loader("css-loader", ".css", {
    query: { localIdentName: "[name]-[local]--[hash:base64:5]" },
  })
  .loader("url-loader", [".jpg", ".png"], {
    query: { limit: 8192 }, // Inline base64 URLs for <= 8K images
  })
  .modules("./lib")
  .plugin("webpack.NamedModulesPlugin")
  .plugin("webpack.ProvidePlugin", { React: "react" })
  .when("development", function(api) {
    return api
      .sourcemap('eval')
      .loader("babel-loader", [".js",".jsx"], {
        exclude: path.resolve(process.cwd(), 'node_modules'),
        query: {
          cacheDirectory: true,
          compact: true,
          presets: [
            "react-hmre",
            "es2015",
            "react",
            "stage-0"
          ],
          plugins: [
          ]
        }
      })

      //.plugin("npm-install-webpack-plugin")
      .plugin("webpack.HotModuleReplacementPlugin")
      .plugin("webpack.NoErrorsPlugin")
    ;
  })
;
