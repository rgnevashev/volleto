/* eslint-disable */
const webpack = require('webpack') //to access built-in plugins
const HtmlWebpackPlugin = require('html-webpack-plugin')
const ExternalsPlugin = require('webpack-externals-plugin')

const path = require('path')
const fs = require('fs')

const intel = require('intel');
intel.addHandler(new intel.handlers.File('./webpack.log'));

/*
const config = require("./webpack.config.defaults")
  .context(path.resolve(process.cwd(), 'server'))
  .entry({
    "server": "./index"
  })
  .target("node")
  .externals(/^@?\w[a-z\-0-9\./]+$/)
  .output({
    "path": path.resolve(process.cwd(), 'compiled'),
    "filename": "[name].js",
    "publicPath": "/assets/",
    "libraryTarget": "commonjs2"
  })
  .when("development", function(api) {
    return api
      .output({
        "path": path.resolve(process.cwd(), 'compiled'),
        "filename": "[name].dev.js",
        "publicPath": "/assets/",
        "libraryTarget": "commonjs2"
      })

      .plugin("start-server-webpack-plugin")
      .plugin("webpack.BannerPlugin", {
        banner: `require("source-map-support").install();`,
        raw: true,
      })
    ;
  })
  .getConfig()
;
*/

const generateConfig = require('@easy-webpack/core').generateConfig

const baseConfig = {
  devtool: "source-map",

  entry: path.resolve(__dirname, 'server/main'),

  output: {
    path: path.resolve(__dirname, 'dist'),
    filename: 'server.bundle.js',
    sourceMapFilename: 'server.map',
    libraryTarget: 'commonjs2' // ?
  },

  target: 'node',

  node: {
    __filename: true,
    __dirname: true,
  },

  resolve: {
    extensions: ['.js', '.jsx', '.css'],
    modules: ['node_modules', path.resolve(__dirname, 'server')]
  },

  module: {
    rules: [{
      test: /\.(js|jsx)$/,
      loader: 'babel-loader',
      exclude: /node_modules/,
      query: {
        cacheDirectory: true
      }
    }]
  },

  externals: fs.readdirSync('node_modules')
    .filter(x => ['.bin'].indexOf(x) === -1)
    .reduce((acc, cur) => Object.assign(acc, { [cur]: 'commonjs ' + cur }), {})
  ,

  plugins: [
    /*
    new ExternalsPlugin({
      type: 'commonjs',
      include: path.resolve(__dirname, 'node_modules'),
    }),*/
    //new webpack.optimize.UglifyJsPlugin(),
    /*
    new HtmlWebpackPlugin({
      filename: path.resolve('public', 'index.html')
    })*/
  ],
}

const config = generateConfig(

  baseConfig,

  require('@easy-webpack/config-css')({
    filename: 'style.css',
    allChunks: true,
    sourceMap: true
  }),

  require('@easy-webpack/config-fonts-and-images')(),

  require('@easy-webpack/config-json')()

  // require('@easy-webpack/config-source-map-support')(),

  // require('@easy-webpack/config-env-development')()
)

delete config.metadata

intel.debug('server %j', config)

module.exports = config
