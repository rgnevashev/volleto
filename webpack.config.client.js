/* eslint-disable */
const HtmlWebpackPlugin = require('html-webpack-plugin'); //installed via npm
const webpack = require('webpack'); //to access built-in plugins
const path = require('path');

const intel = require('intel');
intel.addHandler(new intel.handlers.File('./webpack.log'));

/*
const config = require("./webpack.config.defaults")
  .context(path.resolve(process.cwd(), 'app'))
  .entry({
    app: [
      "./client"
    ]
  })
  .output({
    "path": path.resolve(process.cwd(), 'public', 'assets'),
    "filename": "[name].js",
    "chunkFilename": "[name].[chunkhash:6].js",
    "publicPath": "/assets/"
  })

  .target("web")
  .when("development", function(api) {
    return api
      .entry({
        app: [
          "./client",
          "webpack-hot-middleware/client?path=/__webpack_hmr&timeout=20000&reload=true"
        ]
      })
      .output({
        "path": path.resolve(process.cwd(), 'public', 'assets'),
        "filename": "[name].js",
        "publicPath": "/assets/"
      })
    ;
  })
  .getConfig()
;

delete config.module.preLoaders

module.exports = config
*/

const generateConfig = require('@easy-webpack/core').generateConfig

const baseConfig = {
  devtool: "cheap-module-eval-source-map",
  //the base directory (absolute path) for resolving the entry option
  //context: __dirname,
  entry: [
    //'eventsource-polyfill',
    //'webpack-hot-middleware/client',
    //'webpack/hot/only-dev-server',
    //'react-hot-loader/patch',
    './app/main'
  ],

  output: {
    path: path.resolve(__dirname, 'public/assets'),
    filename: "app.bundle.js",
    publicPath: '/assets/',
    sourceMapFilename: 'app.map'
  },

  resolve: {
    modules: [path.resolve(__dirname, 'app'), 'node_modules'],
    extensions: ['.js', '.jsx', '.css']
  },

  module: {
    rules: [
      {
        test: /\.(js|jsx)$/,
        loader: 'babel-loader',
        exclude: /node_modules/,
        query: {
          cacheDirectory: true
        }
      },
      /*
      {
        test: /\.css$/,
        include: /node_modules/,
        use: ['style-loader', 'css-loader']
      }*/
    ]
  },
  target: "web",
  plugins: [
    new webpack.HotModuleReplacementPlugin(),
    new webpack.DefinePlugin({
      'process.env': {
        CLIENT: JSON.stringify(true),
        'NODE_ENV': JSON.stringify('development'),
      }
    }),
    /*
    new webpack.ProvidePlugin({
      //React: 'react'
    }),*/
    /*
    new HtmlWebpackPlugin({
      filename: '../index.html'
    })*/
  ],
}

const config = generateConfig(

  baseConfig,

  require('@easy-webpack/config-css')({
    filename: 'style.css',
    allChunks: true,
    sourceMap: true
  }),

  require('@easy-webpack/config-fonts-and-images')(),

  require('@easy-webpack/config-json')()

  // require('@easy-webpack/config-source-map-support')(),

  // require('@easy-webpack/config-env-development')()
)

delete config.metadata

intel.debug('client %j', config)

module.exports = config
